package com.thecrackertechnology.dragonterminal.component.extrakey

import android.content.Context
import android.os.Handler
import android.support.v4.app.ActivityCompat.finishAffinity
import io.neolang.visitor.ConfigVisitor
import com.thecrackertechnology.andrax.AndraxApp
import com.thecrackertechnology.dragonterminal.frontend.component.helper.ConfigFileBasedComponent
import com.thecrackertechnology.dragonterminal.frontend.config.NeoTermPath
import com.thecrackertechnology.dragonterminal.frontend.logging.NLog
import com.thecrackertechnology.dragonterminal.frontend.terminal.extrakey.ExtraKeysView
import com.thecrackertechnology.dragonterminal.utils.AssetsUtils
import java.io.File
import com.thecrackertechnology.dragonterminal.component.extrakey.NeoExtraKey
import com.thecrackertechnology.dragonterminal.ui.term.NeoTermActivity


/**
 * @author kiva
 */
class ExtraKeyComponent : ConfigFileBasedComponent<NeoExtraKey>(AndraxApp.get().filesDir.absolutePath + "/home/.neoterm/eks") {
    override val checkComponentFileWhenObtained
            get() = true

    private val extraKeys: MutableMap<String, NeoExtraKey> = mutableMapOf()

    override fun onCheckComponentFiles() {
        val defaultFile = File(AndraxApp.get().filesDir.absolutePath+"/home/.neoterm/eks/default.nl")
        if (!defaultFile.exists()) {

            extractDefaultConfig(AndraxApp.get())

        }

        reloadExtraKeyConfig()

    }

    override fun onCreateComponentObject(configVisitor: ConfigVisitor): NeoExtraKey {
        return NeoExtraKey()
    }

    fun showShortcutKeys(program: String, extraKeysView: ExtraKeysView?) {
        if (extraKeysView == null) {
            return
        }

        val extraKey = extraKeys[program]
        if (extraKey != null) {
            extraKey.applyExtraKeys(extraKeysView)
            return
        }

        extraKeysView.loadDefaultUserKeys()
    }

    private fun registerShortcutKeys(extraKey: NeoExtraKey) =
            extraKey.programNames.forEach {
                extraKeys[it] = extraKey
            }

    private fun extractDefaultConfig(context: Context) {
        try {
            AssetsUtils.extractAssetsDir(context, "eks", AndraxApp.get().filesDir.absolutePath+"/home/.neoterm/eks")
        } catch (e: Exception) {
            NLog.e("ExtraKey", "Failed to extract configure: ${e.localizedMessage}")
        }
    }

    private fun reloadExtraKeyConfig() {
        extraKeys.clear()

        File(AndraxApp.get().filesDir.absolutePath+"/home/.neoterm/eks")
                .listFiles(NEOLANG_FILTER)
                .filter { it.absolutePath != AndraxApp.get().filesDir.absolutePath+"/home/.neoterm/eks" }
                .mapNotNull { this.loadConfigure(it) }
                .forEach {
                    registerShortcutKeys(it)
                }


    }
}